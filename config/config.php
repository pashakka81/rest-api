<?php
return [
    'id' => 'rest-api',
    'name' => 'REST API',
    'language' => 'en',
    'basePath' => __DIR__ . '/../',
    'controllerNamespace' => 'restapi\controllers',
    'aliases' => [
        '@restapi' => __DIR__ . '/../',
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'rest' => [
            'class' => 'restapi\modules\rest\Module',
        ],
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'POST rest/images/upload' => 'rest/image/upload',
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'DCPOCUiCJ4cT-c13FPEvS9grbvq8FeAn',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'thumbnail' => [
            'class' => 'himiklab\thumbnail\EasyThumbnail',
            'cacheAlias' => 'assets/gallery_thumbnails',
        ],
    ],
];

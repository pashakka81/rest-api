<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2/11/19
 * Time: 12:23 AM
 */

namespace restapi\modules\rest\helpers;

/**
 * Helper for working with images. Supports jpg and png image types.
 * @package restapi\modules\rest\helpers
 */
class ImageHelper
{
    const IMAGE_TYPE_JPEG = 1;
    const IMAGE_TYPE_PNG = 2;

    /**
     * @var array Supported image types
     */
    private $types = [
        self::IMAGE_TYPE_JPEG => 'isJpeg',
        self::IMAGE_TYPE_PNG => 'isPng',
    ];

    /**
     * Whether the image has a jpeg type
     * @param $image Image data
     * @return bool
     */
    function isJpeg($image)
    {
        return (bin2hex($image[0]) == 'ff' && bin2hex($image[1]) == 'd8');
    }

    /**
     * Whether the image has a png type
     * @param $image Image data
     * @return bool
     */
    function isPng($image)
    {
        return (bin2hex($image[0]) == '89' && $image[1] == 'P' && $image[2] == 'N' && $image[3] == 'G');
    }

    /**
     * Whether the data is a valid image of the specified types
     * @param $image Image data
     * @param array $types List of valid image types
     * @return bool
     */
    function isImage($image, $types = [self::IMAGE_TYPE_JPEG, self::IMAGE_TYPE_PNG])
    {
        foreach ($types as $type) {
            if (isset($this->types[$type]) && call_user_func([$this, $this->types[$type]], $image)) {
                return true;
            }
        }

        return false;
    }
}

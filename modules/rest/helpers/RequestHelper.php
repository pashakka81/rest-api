<?php

namespace restapi\modules\rest\helpers;

/**
 * Request Helper
 * @package restapi\modules\rest\helpers
 */
class RequestHelper
{
    /**
     * Content type constants
     */
    const CONTENT_TYPE_UNKNOWN = 0;
    const CONTENT_TYPE_APPLICATION_JSON = 1;
    const CONTENT_TYPE_APPLICATION_X_WWW_FORM_URLENCODED = 2;
    const CONTENT_TYPE_MULTIPART_FORM_DATA = 3;
    
    /**
     * Detect content type by http header sting
     * @param $header string Header string from http request
     */
    public static function detectContentType(string $header)
    {
        if ($header == 'application/json') {
            return self::CONTENT_TYPE_APPLICATION_JSON;
        } elseif ($header == 'application/x-www-form-urlencoded') {
            return self::CONTENT_TYPE_APPLICATION_X_WWW_FORM_URLENCODED;
        } elseif (strpos($header, 'multipart/form-data') === 0) {
            return self::CONTENT_TYPE_MULTIPART_FORM_DATA;
        } else {
            return self::CONTENT_TYPE_UNKNOWN;
        }
    }
}

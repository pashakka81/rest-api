<?php

namespace restapi\modules\rest\controllers;

use restapi\modules\rest\Module;
use restapi\modules\rest\services\image\ReceiverService;
use restapi\modules\rest\services\image\UploaderService;
use yii\rest\Controller;

/**
 * Class ImageController
 * @package restapi\modules\rest\controllers
 */
class ImageController extends Controller
{
    /**
     * Response statuses
     */
    const STATUS_OK = 'OK';
    const STATUS_ERROR = 'ERROR';

    /**
     * @var ReceiverService
     */
    private $receiverService;

    /**
     * @var UploaderService
     */
    private $uploaderService;

    /**
     * @return array
     */
    public function behaviors()
    {
        // delete rateLimiter, needed for authenticate users
        $behaviors = parent::behaviors();
        unset($behaviors['rateLimiter']);
        return $behaviors;
    }

    /**
     * ImageController constructor.
     * @param $id
     * @param Module $module
     * @param array $config
     * @param ReceiverService $receiverService
     * @param UploaderService $uploaderService
     */
    public function __construct(
        $id,
        Module $module,
        array $config = [],
        ReceiverService $receiverService,
        UploaderService $uploaderService
    ) {
        // inject services via dependency injection
        $this->receiverService = $receiverService;
        $this->uploaderService = $uploaderService;
        parent::__construct($id, $module, $config);
    }

    /**
     * Action to upload images
     * There are three types of queries are supported:
     * 1) Content type: application/json
     *    Contains json array of base64-encoded images.
     *    Example: ["data:image\/jpeg;base64,image_bytes","data:image\/jpeg;base64,image_bytes","data:image\/jpeg;base64,image_bytes"]
     *
     * 2) Content-type: application/x-www-form-urlencoded
     *    Contains image urls from the internet. Urls are usual urlencoded string. The name of the parameter is 'files[]'.
     *    Example: files%5B%5D=url_to_image&files%5B%5D=url_to_image...
     *
     * 3) Content-type: multipart/form-data
     *    Image files must be use 'files[]' param names.
     *
     * A thumbnail of 100x100 size is created automatically.
     *
     * @return array Json object contains response status and error text.
     */
    public function actionUpload()
    {
        try {
            // receive files from request
            $files = $this->receiverService->receive(\Yii::$app->request->contentType, \Yii::$app->request->bodyParams);

            // upload files to storage
            $this->uploaderService->upload($files);

            // good response
            $response = [
                'status' => self::STATUS_OK,
                'errorText' => '',
            ];
        } catch (\Exception $e) {
            // bad response
            $response = [
                'status' => self::STATUS_ERROR,
                'errorText' => $e->getMessage(),
            ];
        }

        return $response;
    }
}

<?php

namespace restapi\modules\rest\services\image;

use yii\db\Exception;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * Save uploaded images to the file storage
 * @package restapi\modules\rest\services\image
 */
class UploaderService
{
    public $path = '@app/files';

    public function __construct()
    {
        $this->path = \Yii::getAlias($this->path);
        FileHelper::createDirectory($this->path);
    }

    /**
     * Save uploaded images to the file storage and create a thumbnail 100x100 size
     * @param $files
     * @throws Exception
     */
    public function upload($files)
    {
        foreach ($files as $file) {
            $fileName = md5($file);
            $filePath = "{$this->path}/{$fileName}";
            if (!file_put_contents($filePath, $file)) {
                throw new Exception('Error uploading file');
            }

            // create thumbnail
            Image::thumbnail($filePath, 100, 100)->save("{$filePath}.jpg");
        }
    }
}

<?php

namespace restapi\modules\rest\services\image;


use restapi\modules\rest\helpers\ImageHelper;
use restapi\modules\rest\helpers\RequestHelper;
use yii\db\Exception;
use yii\web\UploadedFile;

/**
 * Receive image data from request
 * @package restapi\modules\rest\services\image
 */
class ReceiverService
{
    /**
     * @var ImageHelper
     */
    private $imageHelper;

    /**
     * ReceiverService constructor.
     * @param ImageHelper $imageHelper
     */
    public function __construct(ImageHelper $imageHelper)
    {
        // inject ImageHelper via constructor (DI)
        $this->imageHelper = $imageHelper;
    }

    /**
     * Receives images data from the request and returns them as an array
     * @param string $contentType
     * @param array $bodyParams
     * @return array
     * @throws Exception
     */
    public function receive(string $contentType, array $bodyParams)
    {
        switch (RequestHelper::detectContentType($contentType)) {
            case RequestHelper::CONTENT_TYPE_APPLICATION_JSON:
                $files = $this->base64Receiver($bodyParams);
                break;
            case RequestHelper::CONTENT_TYPE_APPLICATION_X_WWW_FORM_URLENCODED:
                $files = $this->urlReceiver($bodyParams);
                break;
            case RequestHelper::CONTENT_TYPE_MULTIPART_FORM_DATA:
                $files = $this->fileReceiver();
                break;
            default:
                throw new Exception('Content type is undefined');
        }

        return $files;
    }

    /**
     * Receives images data from the json request
     * @param $bodyParams
     * @return array
     * @throws Exception
     */
    private function base64Receiver($bodyParams)
    {
        $files = [];

        foreach ($bodyParams as $param) {
            $param = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $param));
            if ($param) {
                if ($this->imageHelper->isImage($param)) {
                    $files[] = $param;
                } else {
                    throw new Exception('File type is not image');
                }
            }
        }

        return $files;
    }

    /**
     * Receives images data from the x-www-form-urlencoded request
     * @param $bodyParams
     * @return array
     * @throws Exception
     */
    private function urlReceiver($bodyParams)
    {
        $files = [];

        if (isset($bodyParams['files'])) {
            foreach ($bodyParams['files'] as $url) {
                if (filter_var($url, FILTER_VALIDATE_URL)) {
                    $file = file_get_contents($url);
                    if ($this->imageHelper->isImage($file)) {
                        $files[] = $file;
                    } else {
                        throw new Exception('File type is not image');
                    }
                }
            }
        }

        return $files;
    }

    /**
     * Receives images data from the multipart/form-data http request
     * @return array
     * @throws Exception
     */
    private function fileReceiver()
    {
        $files = [];

        foreach (UploadedFile::getInstancesByName('files') as $file) {
            $file = file_get_contents($file->tempName);
            if ($this->imageHelper->isImage($file)) {
                $files[] = $file;
            } else {
                throw new Exception('File type is not image');
            }
        }

        return $files;
    }
}

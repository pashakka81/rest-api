<?php

namespace restapi\controllers;

use yii\web\Controller;

/**
 * Site front controller
 * @package restapi\controllers
 */
class SiteController extends Controller
{
    /**
     * Default site action
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Example page contains forms for sending requests to REST API
     * @return string
     */
    public function actionExample()
    {
        return $this->render('example');
    }
}
